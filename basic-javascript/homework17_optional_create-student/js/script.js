"use strict"

const student = {
    name: "",
    "last name": "",
    calcBadGrades() {
        let badGrades = 0;
        for (const prop in this.table) {
            if (this.table[prop] < 4) badGrades++;
        }
        if (badGrades === 0) console.log("Студент переведен на следующий курс");
    },
    calcAvgGrade() {
        let gradeSum = 0;
        let gradeCount = 0;
        let avgGrade;
        for (const prop in this.table) {
            gradeSum += this.table[prop];
            gradeCount++;
        }
        avgGrade = gradeSum / gradeCount;
        if (avgGrade > 7) console.log("Студенту назначена стипендия");
    }
}

student.name = prompt("Enter your name:");
student["last name"] = prompt("Enter your last name:");
student.table = {};
do {
    let subjectName = prompt("Enter the name of subject:");
    if (subjectName === null) break;
    student.table[subjectName] = +prompt("Enter a grade for the subject:");
} while (true);
student.calcBadGrades();
student.calcAvgGrade();