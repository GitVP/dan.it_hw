"use strict"

function activateTab() {
        $(this).addClass("active");
        $(this).siblings("li").removeClass("active");
        const matchedContent = $(`.tabs-content > li[data-hero="${$(this).data("hero")}"]`);
        $(matchedContent).show();
        $(matchedContent).siblings("li").hide();
}

const tabs = $(".tabs-title");
tabs.on("click", activateTab);