"use strict"

function slideCycled(sec) {
    sec--;
    ms = 1000;
    secSpan.textContent = sec;
    msSpan.textContent = "000";
    timerId = setInterval(startTimer(sec), 20);
}

function startTimer(sec) {
    return function () {
        if (sec < 0) {
            clearInterval(timerId);
            showNextImage();
            slideCycled(INTERVAL_TIME);
            return;
        }
        if (ms > 0) {
            ms -= 20;
            msSpan.textContent = ms;
        } else {
            ms = 1000;
            sec--;
            if (sec >= 0) secSpan.textContent = sec;
        }
    }
}

function showNextImage() {
    const currentImg = document.querySelector(".image-to-show");
    currentImg.className = "img image-to-hide";
    const newImage = currentImg.nextElementSibling;
    if (newImage) {
        newImage.className = "img image-to-show";
    } else {
        const firstImage = document.querySelector(".img");
        firstImage.className = "img image-to-show";
    }
}


let timerId;
let ms;
const secSpan = document.querySelector(".seconds");
const msSpan = document.querySelector(".milliseconds");
const stopBtn = document.querySelector("button[data-action='stop']");
const continueBtn = document.querySelector("button[data-action='continue']");

stopBtn.addEventListener("click", () => clearInterval(timerId));
continueBtn.addEventListener("click", () => slideCycled(INTERVAL_TIME));

const INTERVAL_TIME = 10;
slideCycled(INTERVAL_TIME);