"use strict"

function createError() {
    validateError.id = "validateError";
    validateError.textContent = "Please enter correct price";
    if (document.querySelector("#spanBtnBox")) spanBtnBox.remove();
    inputPrice.style.outlineWidth = "1px";
    inputPrice.style.outlineStyle = "auto";
    inputPrice.style.outlineColor = "red";
    labelPrice.after(validateError);
}

function createSpanPrice() {
    if (document.querySelector("#validateError")) validateError.remove();
    spanPrice.innerHTML = `Текущая цена: ${inputPrice.value} `;
    inputPrice.style.outlineWidth = "0";
    inputPrice.style.color = "green";
    document.body.prepend(spanBtnBox);
}

function createSpanBtnBox() {
    labelPrice.textContent = "Price: ";
    labelPrice.appendChild(inputPrice);
    spanBtnBox.id = "spanBtnBox";
    btnClear.type = "button";
    btnClear.style.borderRadius = "50%";
    btnClear.textContent = "X";
    spanBtnBox.appendChild(spanPrice);
    spanBtnBox.appendChild(btnClear);
}

const labelPrice = document.createElement("label");
const inputPrice = document.createElement("input");
const spanBtnBox = document.createElement("p");
const spanPrice = document.createElement("span");
const btnClear = document.createElement("button");
const validateError = document.createElement("p");

createSpanBtnBox();

inputPrice.addEventListener("focus", () => {
    inputPrice.style.outlineColor = "green";
    inputPrice.style.outlineWidth = "1px";
});
inputPrice.addEventListener("blur", () => {
    if (inputPrice.value < 0) {
        createError();
        return;
    }
    createSpanPrice();
});
btnClear.addEventListener("click", () => {
    spanBtnBox.remove();
    inputPrice.value = "";
})

document.body.prepend(labelPrice);