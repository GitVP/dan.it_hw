"use strict"

function createTable(columns, rows) {
    const table = document.createElement("table");
    for (let i = 0; i < rows; i++) {
        const tr = document.createElement("tr");
        for (let j = 0; j < columns; j++) {
            const td = document.createElement("td");
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

const table = createTable(30, 30);
document.body.prepend(table);

table.addEventListener("click", (event) => {
    if (event.target.tagName === "TD") event.target.classList.toggle("active");
});
document.body.addEventListener("click", (event) => {
    if (event.target.tagName === "BODY") table.classList.toggle("revert-table");
});