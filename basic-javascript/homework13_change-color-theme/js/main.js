"use strict"

function setNewTheme() {
    document.body.classList.replace("light-body", "dark-body");
    table.classList.replace("dark-border", "light-border");
    footerBtns.forEach(setNewColor);
    theads.forEach(setNewColor);
    theme = "newTheme";
    localStorage.setItem("theme", theme);
}

function setOldTheme() {
    document.body.classList.replace("dark-body", "light-body");
    table.classList.replace("light-border", "dark-border");
    footerBtns.forEach(setOldColor);
    theads.forEach(setOldColor);
    theme = "oldTheme";
    localStorage.setItem("theme", theme);
}

function setNewColor(elem) {
    for (const oldColor in colorMatches) {
        if (elem.classList.contains(oldColor)) elem.classList.replace(oldColor, colorMatches[oldColor]);
    }
}

function setOldColor(elem) {
    for (const oldColor in colorMatches) {
        if (elem.classList.contains(colorMatches[oldColor])) elem.classList.replace(colorMatches[oldColor], oldColor);
    }
}

function initializePage () {
    if (localStorage.getItem("theme")) {
        theme = localStorage.getItem("theme");
    } else {
        theme = "oldTheme";
        localStorage.setItem("theme", theme);
    }
    theme === "oldTheme" ? setOldTheme() : setNewTheme();
}


const colorMatches = {
    "vdarkgrey-color": "yellow-color",
    "moderate-green-color": "strong-magenta-color",
    "soft-red-color": "bright-blue-color",
    "dark-cyan-color": "vivid-cyan-color"
}

const themeBtn = document.querySelector(".change-theme-button");
const footerBtns = Array.from(document.querySelectorAll(".button-footer"));
const theads = Array.from(document.querySelectorAll("th"));
const table = document.querySelector(".table");
let theme;

document.addEventListener("DOMContentLoaded", initializePage);
themeBtn.addEventListener("click", () => {
    theme === "oldTheme" ? setNewTheme() : setOldTheme();
})