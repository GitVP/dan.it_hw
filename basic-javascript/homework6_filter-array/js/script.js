"use strict"

function filterBy(arr, dataType) {
    const newArr = [];
    if (dataType === "null") {
        arr.forEach((value) => {
            if (value !== null) newArr.push(value);
        });
    } else {
        arr.forEach((value) => {
            if (typeof value !== dataType) newArr.push(value);
        });
    }
    return newArr;
}

const arr = ['hello', 'world', 23, '23', null];
console.log(filterBy(arr, "string"));