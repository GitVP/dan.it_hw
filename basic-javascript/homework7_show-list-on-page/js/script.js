"use strict"

function showListOnPage(arr) {
    const list = document.createElement("ul");
    document.body.prepend(list);
    const liArr = arr.map(item => {
        if (item instanceof Array) {
            const nestedList = document.createElement("ul");
            item.forEach(nestedItem => {
                const newItem = document.createElement("li");
                newItem.textContent = nestedItem;
                nestedList.appendChild(newItem);
            });
            return nestedList;
        }
        if (item instanceof Object) {
            const nestedList = document.createElement("ul");
            for (const itemKey in item) {
                const newItem = document.createElement("li");
                newItem.textContent = `${itemKey}: ${item[itemKey]}`;
                nestedList.appendChild(newItem);
            }
            return nestedList;
        }
        const newItem = document.createElement("li");
        newItem.textContent = item;
        return newItem;
    });
    list.append(...liArr);
    clearAfterTime(10);
}

function clearAfterTime(seconds) {
    document.body.insertAdjacentHTML("beforeend", `<p>До очистки осталось:</p><span>${seconds}с</span>`);
    const timerSec = document.querySelector("p + span");
    const timer = setInterval(() => {
        if (seconds <= 0) {
            clearInterval(timer);
            document.body.innerHTML = "";
        } else timerSec.innerText = `${--seconds}с`;
    }, 1000);
}

const cities = ['hello', ['world', 'dlrow'], 'Kharkiv', {name: 'Kiev', surname: 'vieK'}, 'Odessa', 'Lviv'];
showListOnPage(cities);