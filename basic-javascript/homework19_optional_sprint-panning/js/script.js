"use strict"

function pannSprint(workSpeed, backlog, deadlineDate) {
    let workSpeedPerDay = 0;
    let backlogTotal = 0;
    let storyPointDone = 0;
    for (const workSpeedElement of workSpeed) {
        workSpeedPerDay += workSpeedElement;
    }
    for (const backlogElement of backlog) {
        backlogTotal += backlogElement;
    }
    let currentMS = Date.now();
    while (currentMS < deadlineDate.getTime()) {
        const currentDay = new Date(currentMS).getDay();
        if (currentDay !== 0 && currentDay !== 6) {
            storyPointDone += workSpeedPerDay;
        }
        currentMS += 1000 * 60 * 60 * 24;
    }
    if (storyPointDone >= backlogTotal) {
        const extraDays = (storyPointDone - backlogTotal) / workSpeedPerDay;
        return `Все задачи будут успешно выполнены за ${Math.floor(extraDays)} дней до наступления дедлайна!`;
    } else {
        const extraHours = (backlogTotal - storyPointDone) / workSpeedPerDay * 8;
        return `Команде разработчиков придется потратить дополнительно ${extraHours.toFixed(2)} часов после дедлайна, чтобы выполнить все задачи в беклоге`;
    }
}

const workSpeedArr = [1, 7, 3, 5, 5];
const backlogArr = [10, 8, 10, 5, 9, 17];
const deadlineDate = new Date("2020 09 05");
console.log(pannSprint(workSpeedArr, backlogArr, deadlineDate));