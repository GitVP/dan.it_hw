"use strict"

function slideHandler(event) {
    if (event.target.classList.contains("arrow-btn")) {
        const imgCount = imgList.children.length;
        const direction = event.target.dataset.direction;
        const currentPos = parseInt(imgList.style.left);
        let stepValue = 0;
        if (direction === "right") {
            stepValue = currentPos === (imgCount - 1) * -100 ? (imgCount - 1) * 100 : -100;
        }
        if (direction === "left") {
            stepValue = currentPos === 0 ? (imgCount - 1) * -100 : 100;
        }
        imgList.style.left = `${currentPos + stepValue}%`;
    }
}

const sliderBox = document.querySelector(".slider-box");
const imgList = document.querySelector(".img-list");
sliderBox.addEventListener("click", slideHandler);