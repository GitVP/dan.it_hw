"use strict"

function createMemorySpan() {
    const memorySpan = document.createElement("span");
    memorySpan.textContent = "m";
    memorySpan.classList.add("memory-span");
    return memorySpan;
}

function buttonPressHandler(event) {
    const elem = event.target;
    const elemContent = elem.dataset.content;
    elem.blur();
    if (elemContent !== "mrc") storedNumberOnDisplay = false;
    if (elemContent === "digit" || elemContent === "dot") {
        if (calcTable.value === "0") calcTable.value = "";
        const digit = elem.value;
        calcTable.value += digit;
        return;
    }
    if (elemContent === "cancel") {
        calcTable.value = "0";
        return;
    }
    if (elemContent === "operation") {
        if (isOperationEntered()) {
            calcTable.value = calc(calcTable.value);
        }
        calcTable.value += ` ${elem.dataset.operation} `;
        return;
    }
    if (elemContent === "equal") {
        calcTable.value = calc(calcTable.value);
        return;
    }
    if (elemContent === "m+") {
        storedNumber += +calcTable.value;
        calcTable.parentElement.append(memorySpan);
        return;
    }
    if (elemContent === "m-") {
        storedNumber -= +calcTable.value;
        calcTable.parentElement.append(memorySpan);
        return;
    }
    if (elemContent === "mrc") {
        if (storedNumberOnDisplay) {
            if (calcTable.parentElement.contains(memorySpan)) memorySpan.remove();
            storedNumber = null;
        }
        if (storedNumber !== null) {
            calcTable.value = String(storedNumber);
            storedNumberOnDisplay = true;
        }
    }
}

function keyboardPressHandler(event) {
    const digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    const operations = ["+", "-", "*", "/"];
    if (digits.some(digit => digit === event.key)) {
        if (calcTable.value === "0") calcTable.value = "";
        calcTable.value += event.key;
        return
    }
    if (operations.some(operation => operation === event.key)) {
        if (isOperationEntered()) {
            calcTable.value = calc(calcTable.value);
        }
        calcTable.value += ` ${event.key} `;
        return;
    }
    if (event.key === "Enter") {
        calcTable.value = calc(calcTable.value);
        return;
    }
    if (event.key === "Delete") calcTable.value = "0";
}

const isOperationEntered = () => calcTable.value.split(" ").length >= 3;

function calc(value) {
    const splitValue = value.split(" ");
    const firstNum = +splitValue[0];
    const operation = splitValue[1];
    const secondNum = +splitValue[2];
    let result;
    switch (operation) {
        case "+": {
            result = firstNum + secondNum;
            break;
        }
        case "-": {
            result = firstNum - secondNum;
            break;
        }
        case "*": {
            result = firstNum * secondNum;
            break;
        }
        case "/": {
            result = (firstNum / secondNum).toFixed(3);
            break;
        }
        default:
            result = +value;
    }
    return result;
}


const buttons = document.querySelector(".keys");
const calcTable = document.querySelector(".display > input");
const memorySpan = createMemorySpan();
let storedNumber = null;
let storedNumberOnDisplay = false;

buttons.addEventListener("click", buttonPressHandler);
document.addEventListener("keyup", keyboardPressHandler);