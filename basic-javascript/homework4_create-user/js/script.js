"use strict"

function createNewUser() {
    const newUser = {
        setFirstName(value) {
            Object.defineProperty(this, "firstName", {writable: true});
            this.firstName = value;
            Object.defineProperty(this, "firstName", {writable: false});
        },
        setLastName(value) {
            Object.defineProperty(this, "lastName", {writable: true});
            this.firstName = value;
            Object.defineProperty(this, "lastName", {writable: false});
        },
        getLogin() {
            return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
        }
    };
    Object.defineProperty(newUser, "firstName", {
        configurable: true,
        value: prompt("Enter your first name:")
    });
    Object.defineProperty(newUser, "lastName", {
        configurable: true,
        value: prompt("Enter your last name:")
    });
    return newUser;
}

const newUser = createNewUser();
console.log(`your login: ${newUser.getLogin()}`);