function calcFactorial(number) {
    if (number === 0) return 1;
    if (number > 0) return (number * calcFactorial(number - 1));
    return "error";
}


let userNumber = prompt("Enter your number");
while (!Number(userNumber) && userNumber !== "0") {
    userNumber = prompt("Please, enter correct number:", userNumber);
}
alert(`${userNumber}! = ${calcFactorial(+userNumber)}`);