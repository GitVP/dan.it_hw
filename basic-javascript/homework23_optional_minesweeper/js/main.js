"use strict"

const resetBtn = document.querySelector("#reset-btn");
const mineStats = document.querySelector(".minebox-stats");
const mineBox = document.querySelector(".minebox");
const bombNumberField = mineStats.querySelector(".bomb-number");
const flagCountField = mineStats.querySelector(".flag-counter");

const fieldSize = +prompt("Введите размер поля:");
let table;
let matrix;

resetBtn.addEventListener("click", initializeField);

initializeField();



function initializeField() {
    if (document.body.contains(table)) table.remove();
    if (!resetBtn.hasAttribute("hidden")) resetBtn.setAttribute("hidden", "");
    table = createTable(fieldSize);
    matrix = getMatrix(table);
    setBombs();
    mineBox.style.width = `${40 * fieldSize}px`;
    mineBox.style.height = `${40 * fieldSize + 70}px`;
    bombNumberField.textContent = String(calcGeneralBombCount());
    flagCountField.textContent = "0";
    mineStats.after(table);
    table.addEventListener("click", cellClickHandler);
    table.addEventListener("contextmenu", cellContextMenuHandler);
    table.addEventListener("dblclick", cellDblClickHandler);
}

function createTable(size) {
    const table = document.createElement("table");
    for (let i = 0; i < size; i++) {
        const tr = document.createElement("tr");
        for (let j = 0; j < size; j++) {
            const td = document.createElement("td");
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    return table;
}

function getMatrix(table) {
    const matrix = [];
    const tRows = table.querySelectorAll("tr");
    tRows.forEach((row) => {
        matrix.push(Array.from(row.children));
    });
    return matrix;
}

function calcBombPositions(number) {
    const arr = [];
    while (arr.length < number) {
        const position = Math.floor(Math.random() * (fieldSize ** 2));
        if (arr.indexOf(position) !== -1) continue;
        arr.push(position)
    }
    return arr;
}

function createBombImage() {
    const bombImg = document.createElement("img");
    bombImg.setAttribute("src", "images/bomb.png");
    bombImg.setAttribute("alt", "bomb");
    bombImg.setAttribute("width", "32");
    bombImg.setAttribute("height", "32");
    bombImg.setAttribute("data-content", "bomb");
    return bombImg;
}

function createFlagImage() {
    const flagImg = document.createElement("img");
    flagImg.setAttribute("src", "images/flag.png");
    flagImg.setAttribute("alt", "flag");
    flagImg.setAttribute("width", "32");
    flagImg.setAttribute("height", "32");
    flagImg.setAttribute("data-content", "flag");
    return flagImg;
}

function calcGeneralBombCount() {
    return Math.floor(fieldSize ** 2 / 6);
}

function setBombs() {
    const bombPositions = calcBombPositions(calcGeneralBombCount());
    const tds = table.querySelectorAll("td");
    bombPositions.forEach(posItem => {
        tds[posItem].append(createBombImage());
    })
}

function getColNumber(cell) {
    return cell.cellIndex;
}

function getRowNumber(cell) {
    return matrix.findIndex(tr => tr.some(td => td === cell));
}

function cellClickHandler(event) {
    if (event.target.tagName === "TD") {
        if (resetBtn.hasAttribute("hidden")) resetBtn.removeAttribute("hidden");
        const selectedTD = event.target;
        if (checkBomb(selectedTD)) {
            startLosingAftermath();
        } else {
            openCells(getRowNumber(selectedTD), getColNumber(selectedTD));
        }
        if (checkWin()) setTimeout(() => alert("Вы победили!"));
    }
}

function cellContextMenuHandler(event) {
    event.preventDefault();
    let flagCount = +flagCountField.textContent;
    if (event.target.tagName === "TD") {
        const selectedTD = event.target;
        if (!isOpened(selectedTD)) {
            selectedTD.append(createFlagImage());
            flagCount++;
        }
    }
    if (event.target.tagName === "IMG") {
        event.target.remove();
        flagCount--;
    }
    flagCountField.textContent = String(flagCount);
    if (checkWin()) setTimeout(() => alert("Вы победили!"));
}

function cellDblClickHandler(event) {
    if (event.target.tagName === "TD") {
        const selectedTD = event.target;
        if (calcBombAround(selectedTD) === calcFlagAround(selectedTD)) openNearbyCells(selectedTD);
    }
}

function startLosingAftermath() {
    table.classList.add("show-all-cells");
    setTimeout(() => alert("Вы проиграли!"), 10);
    table.removeEventListener("click", cellClickHandler);
    table.removeEventListener("contextmenu", cellContextMenuHandler);
    table.removeEventListener("dblclick", cellDblClickHandler);
}

function checkWin() {
    return matrix.every(tr => tr.every(td => {
        return td.classList.contains("active") || td.querySelector("img[data-content='flag']");
    }))
}

function removeFlag(cell) {
    const flag = cell.querySelector("img[data-content='flag']");
    flag.remove();
    let flagCount = +flagCountField.textContent;
    flagCount--;
    flagCountField.textContent = String(flagCount);
}

function checkFlag(rowN, colN) {
    if (rowN >= 0 && rowN <= (fieldSize - 1) && colN >= 0 && colN <= (fieldSize - 1)) {
        const cell = matrix[rowN][colN];
        if (cell.querySelector("img[data-content='flag']")) return true;
    }
}

function calcFlagAround(cell) {
    const colN = getColNumber(cell);
    const rowN = getRowNumber(cell);
    let flagCount = 0;
    if (checkFlag(rowN - 1, colN - 1)) flagCount++;
    if (checkFlag(rowN - 1, colN)) flagCount++;
    if (checkFlag(rowN - 1, colN + 1)) flagCount++;
    if (checkFlag(rowN, colN - 1)) flagCount++;
    if (checkFlag(rowN, colN + 1)) flagCount++;
    if (checkFlag(rowN + 1, colN - 1)) flagCount++;
    if (checkFlag(rowN + 1, colN)) flagCount++;
    if (checkFlag(rowN + 1, colN + 1)) flagCount++;
    return flagCount;
}

function showNearbyCell(rowN, colN) {
    if (rowN >= 0 && rowN <= (fieldSize - 1) && colN >= 0 && colN <= (fieldSize - 1)) {
        const cell = matrix[rowN][colN];
        if (checkBomb(cell) && checkFlag(rowN, colN)) removeFlag(cell);
        showCell(matrix[rowN][colN]);
    }
}

function openNearbyCells(cell) {
    const colN = getColNumber(cell);
    const rowN = getRowNumber(cell);
    showNearbyCell(rowN - 1, colN - 1);
    showNearbyCell(rowN - 1, colN);
    showNearbyCell(rowN - 1, colN + 1);
    showNearbyCell(rowN, colN - 1);
    showNearbyCell(rowN, colN + 1);
    showNearbyCell(rowN + 1, colN - 1);
    showNearbyCell(rowN + 1, colN);
    showNearbyCell(rowN + 1, colN + 1);
}

function showCell(cell) {
    cell.classList.add("active");
}

function isOpened(cell) {
    return cell.classList.contains("active");
}

function openCells(row, column) {
    if (row < 0 || row > (fieldSize - 1) || column < 0 || column > (fieldSize - 1)) return;
    const cell = matrix[row][column];
    if (checkBomb(cell)) return;
    if (calcBombAround(cell) === 0 && !isOpened(cell)) {
        showCell(cell);
        openCells(row - 1, column - 1);
        openCells(row - 1, column);
        openCells(row - 1, column + 1);
        openCells(row, column - 1);
        openCells(row, column + 1);
        openCells(row + 1, column - 1);
        openCells(row + 1, column);
        openCells(row + 1, column + 1);
    }
    showCell(cell);
    if (cell.querySelector("img[data-content='flag']")) removeFlag(cell);
    cell.textContent = calcBombAround(cell) || "";
}

function checkBomb(...args) {
    let cell;
    if (args.length === 1) {
        cell = args[0];
        if (cell.querySelector("img[data-content='bomb']")) return true;
    }
    if (args.length === 2 && args[0] >= 0 && args[0] <= (fieldSize - 1) && args[1] >= 0 && args[1] <= (fieldSize - 1)) {
        cell = matrix[args[0]][args[1]];
        if (cell.querySelector("img[data-content='bomb']")) return true;
    }
    return false;
}

function calcBombAround(cell) {
    const colN = getColNumber(cell);
    const rowN = getRowNumber(cell);
    let bombCount = 0;
    if (checkBomb(rowN - 1, colN - 1)) bombCount++;
    if (checkBomb(rowN - 1, colN)) bombCount++;
    if (checkBomb(rowN - 1, colN + 1)) bombCount++;
    if (checkBomb(rowN, colN - 1)) bombCount++;
    if (checkBomb(rowN, colN + 1)) bombCount++;
    if (checkBomb(rowN + 1, colN - 1)) bombCount++;
    if (checkBomb(rowN + 1, colN)) bombCount++;
    if (checkBomb(rowN + 1, colN + 1)) bombCount++;
    return bombCount;
}