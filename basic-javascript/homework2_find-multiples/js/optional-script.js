let firstNumber = +prompt("Enter first number:");
let secondNumber = +prompt("Enter second number:");
let lessNumber;
let largerNumber;

while (!Number.isInteger(firstNumber) || !Number.isInteger(secondNumber)) {
    firstNumber = +prompt("Please, enter integer first number:");
    secondNumber = +prompt("Please, enter integer second number:");
}
if (firstNumber >= secondNumber) {
    lessNumber = secondNumber;
    largerNumber = firstNumber;
} else {
    lessNumber = firstNumber;
    largerNumber = secondNumber;
}
outer:for (let currentNumber = lessNumber; currentNumber <= largerNumber; currentNumber++) {
    for (let i = 2; i < currentNumber; i++) {
        if (currentNumber % i === 0) {
            continue outer;
        }
    }
    console.log(currentNumber);
}