let userNumber = +prompt("Enter your number:");
let multipleCounter = 0;

while (!Number.isInteger(userNumber)) {
    userNumber = +prompt("Please, enter integer number:");
}
for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
        console.log(i);
        multipleCounter++;
    }
}
if (multipleCounter === 0) {
    console.log("Sorry, no numbers");
}