"use strict"

function getRandomColor() {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    return `rgb(${r}, ${g}, ${b})`;
}

const circleGrid = document.createElement("div");
const drawBtn = document.querySelector("button");

drawBtn.addEventListener("click", () => {
    const diam = prompt("Введите диаметр круга в px:");
    circleGrid.style.cssText = `display: grid; grid-template: repeat(10, ${diam}px) / repeat(10, ${diam}px);`;
    for (let i = 0; i < 100; i++) {
        const circle = document.createElement("div");
        circle.style.cssText = `width: ${diam}px; height: ${diam}px; border-radius: 50%; background-color: ${getRandomColor()};`;
        circleGrid.appendChild(circle);
    }
    document.body.prepend(circleGrid);
});

circleGrid.addEventListener("click", (event) => {
    if ("style" in event.target) {
        if (event.target.style.display !== "grid") event.target.remove();
    }
});