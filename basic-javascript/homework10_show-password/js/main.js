"use strict"

function createErrorMessage(errorText) {
    const errorMessage = document.createElement("p");
    errorMessage.textContent = errorText;
    errorMessage.classList.add("error-message");
    return errorMessage;
}

function showPassword() {
    const passField = this.previousElementSibling;
    if (this.classList.contains("fa-eye")) {
        passField.setAttribute("type", "text");
    } else passField.setAttribute("type", "password");
    this.classList.toggle("fa-eye");
    this.classList.toggle("fa-eye-slash");
}

function checkPasswords() {
    const passFields = document.querySelectorAll("input[data-type='password']");
    if (passFields[0].value === passFields[1].value) {
        if (document.querySelector(".error-message")) errorMessage.remove();
        setTimeout(() => alert("You are welcome"), 10);
    } else submitBtn.before(errorMessage);
}


const form = document.querySelector(".password-form");
const icons = document.querySelectorAll(".icon-password");
const submitBtn = document.querySelector(".btn");
const errorMessage = createErrorMessage("Нужно ввести одинаковые значения");

form.addEventListener("submit", (event) => event.preventDefault());
icons.forEach(icon => icon.addEventListener("click", showPassword));
submitBtn.addEventListener("click", checkPasswords);