"use strict"


function addSmoothScroll(event) {
    event.preventDefault();
    const destination = $(this).attr("href");
    $("html").animate({scrollTop: $(destination).offset().top}, 1000);
}

function scrollHandler() {
    const screenHeight = $(this).height();
    if ($(this).scrollTop() > screenHeight) {
        $(upBtn).show(500);
    } else $(upBtn).hide(500)
}

function toggleBtnHandler() {
    const targetSection = $(this).closest("section").prev();
    $(targetSection).toggleClass("hidden");
    $(targetSection).slideToggle(500);
    const sectionID = $(targetSection[0]).attr("id");
    $(targetSection).hasClass("hidden") ? $(this).text(`показать ${sectionID}`) : $(this).text(`скрыть ${sectionID}`);
}

const upBtn = $(".up-button");
$("a.scroll-to").on("click", addSmoothScroll);
$(".section-toggle-btn").on("click", toggleBtnHandler);
$(window).on("scroll", scrollHandler);