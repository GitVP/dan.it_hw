function mathOp(a, b, op) {
    switch (op) {
        case "+" :
            return (a + b);
        case "-" :
            return (a - b);
        case "*" :
            return (a * b);
        case "/":
            return (a / b);
        default:
            return "error!";
    }
}

function isNumber(number) {
    if (number === "" || number === null || isNaN(+number)) {
        return false;
    } else {
        return true;
    }
}


let num1 = prompt("Enter first number:");
let num2 = prompt("Enter second number:");
while (!isNumber(num1) || !isNumber(num2)) {
    num1 = prompt("Please, enter correct first number:", num1);
    num2 = prompt("Please, enter correct first number:", num2);
}
let operation = prompt("Enter math operation (+, -, *, /):");
console.log(`${num1} ${operation} ${num2} = ${mathOp(+num1, +num2, operation)}`);