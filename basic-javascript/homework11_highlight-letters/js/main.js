"use strict"

function highlightButton(event) {
    const buttons = Array.from(document.querySelectorAll(".btn"));
    const newHighlightBtn = buttons.find(button => button.dataset.code === event.code);
    if (newHighlightBtn) {
        unhighlightButton(buttons);
        newHighlightBtn.style.backgroundColor = "rgb(0, 0, 255)";
    }
}

function unhighlightButton(buttons) {
    const oldHighlightBtn = buttons.find(button => button.style.backgroundColor === "rgb(0, 0, 255)");
    if (oldHighlightBtn) oldHighlightBtn.style.backgroundColor = "rgb(51, 51, 58)";
}

document.addEventListener("keyup", highlightButton);