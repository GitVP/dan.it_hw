function fibonacci(f0, f1, n) {
    if (n < 0) return (fibonacci(f0, f1, n + 2) - fibonacci(f0, f1, n + 1));
    if (n === 0) return f0;
    if (n === 1) return f1;
    return (fibonacci(f0, f1, n - 2) + fibonacci(f0, f1, n - 1));
}

let userNumber = prompt("Enter your number:");
let f0 = prompt("Enter f0:");
let f1 = prompt("Enter f1:");
alert(`starting from f0 = ${f0}, f1 = ${f1}, fibo element №${userNumber} = ${fibonacci(+f0, +f1, +userNumber)}`);