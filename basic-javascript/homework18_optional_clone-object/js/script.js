"use strict"

function cloneDeep(obj) {
    if (!(obj instanceof Object)) return obj;
    if (obj instanceof Array) {
        let copyArr = [];
        for (let i = 0; i < obj.length; i++) {
            copyArr.push(cloneDeep(obj[i]));
        }
        return copyArr;
    }
    if (obj instanceof Object) {
        let copyObj = {};
        for (const prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                copyObj[prop] = cloneDeep(obj[prop]);
            }
        }
        return copyObj;
    }
    return cloneDeep(obj);
}

//test object
const student = {
    name: "Катерина",
    "last name": "Петрова",
    status: null,
    tabel: {
        history: 12,
        biology: 12,
        mathematics: 8,
        physics: 9,
        geography: 7,
        "bad marks": {
            xxx: 1,
            xx1: 2,
            xx2: 3,
            xx3: [4, 5, 6],
            xx4: {
                yy1: 0,
                yy2: -1,
                yy3: -10
            }
        }
    }
};
const clonedObj = cloneDeep(student);

//checking the result
console.log(student);
console.log(clonedObj);