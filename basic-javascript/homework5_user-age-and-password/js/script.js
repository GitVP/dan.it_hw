"use strict"

function createNewUser() {
    const birthday = prompt("Enter your birthday (dd.mm.yyyy):");
    const newUser = {
        birthday: birthday,
        setFirstName(value) {
            Object.defineProperty(this, "firstName", {writable: true});
            this.firstName = value;
            Object.defineProperty(this, "firstName", {writable: false});
        },
        setLastName(value) {
            Object.defineProperty(this, "lastName", {writable: true});
            this.firstName = value;
            Object.defineProperty(this, "lastName", {writable: false});
        },
        getLogin() {
            return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
        },
        getAge() {
            const today = new Date();
            const birthdayDate = new Date(`${this.birthday.slice(-4)} ${this.birthday.slice(3, 5)} ${this.birthday.slice(0, 2)}`);
            if (today.getMonth() >= birthdayDate.getMonth() && today.getDate() >= birthdayDate.getDate()) {
                return (today.getFullYear() - birthdayDate.getFullYear());
            }
            return (today.getFullYear() - birthdayDate.getFullYear() - 1);
        },
        getPassword() {
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4));
        }
    };
    Object.defineProperty(newUser, "firstName", {
        configurable: true,
        value: prompt("Enter your first name:")
    });
    Object.defineProperty(newUser, "lastName", {
        configurable: true,
        value: prompt("Enter your last name:")
    });
    return newUser;
}

const newUser = createNewUser();
console.log(`your login: ${newUser.getLogin()}`);
console.log(`your password: ${newUser.getPassword()}`);
console.log(`your age: ${newUser.getAge()}`);