"use strict"

const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content > li");

tabs.forEach((tabsItem, tabsIndex) => {
    tabsItem.addEventListener("click", () => {
        tabs.forEach((tab) => {
            if (tab.classList.contains("active")) tab.classList.remove("active");
        });
        tabsItem.classList.add("active");
        tabsContent.forEach(tabsContentItem => {
            tabsContentItem.style.display = "none";
        });
        if (tabsIndex in tabsContent) tabsContent[tabsIndex].style.display = "list-item";
    });
});

tabsContent.forEach((item, index) => {
    if (index > 0) item.style.display = "none";
});