"use strict"

function filterCollection(arr, keywords, findAllKeywords, ...restProperties) {
    let result;
    const keywordsArr = keywords.split(" ");
    restProperties.forEach((item, index, array) => array[index] = item.split("."));
    if (findAllKeywords) {
        result = arr.filter(item => {
            let matchCounter = 0;
            for (const restPropertiesElement of restProperties) {
                for (const keywordsArrElement of keywordsArr) {
                    const checkingProp = getProperty(restPropertiesElement, item);
                    if (Array.isArray(checkingProp)) {
                        if (findInArray(checkingProp, restPropertiesElement[restPropertiesElement.length - 1], keywordsArrElement)) matchCounter++;
                    }
                    if (checkEquality(checkingProp, keywordsArrElement)) matchCounter++;
                }
                if (matchCounter === keywordsArr.length) return true;
            }
        });
    } else {
        result = arr.filter(item => {
            for (const restPropertiesElement of restProperties) {
                for (const keywordsArrElement of keywordsArr) {
                    const checkingProp = getProperty(restPropertiesElement, item);
                    if (Array.isArray(checkingProp)) {
                        if (findInArray(checkingProp, restPropertiesElement[restPropertiesElement.length - 1], keywordsArrElement)) return true;
                    }
                    if (checkEquality(checkingProp, keywordsArrElement)) return true;
                }
            }
        });
    }
    return result;
}

function getProperty(propArr, obj) {
    for (let i = 0; i < propArr.length; i++) {
        if (obj.hasOwnProperty(propArr[i])) obj = obj[propArr[i]];
    }
    return obj;
}

function checkEquality(item1, item2) {
    if (typeof item1 === "string") item1 = item1.toLowerCase();
    if (typeof item2 === "string") item2 = item2.toLowerCase();
    return item1 === item2;
}

function findInArray(arr, property, value) {
    let matchCounter = 0;
    arr.forEach(item => {
        if (checkEquality(item[property], value)) matchCounter++
    });
    return matchCounter;
}


const vehicles = [
    {
        name: "Toyota",
        description: [{name: "aaaa", surname: "cccccc"}, {name: "bbbb"}, {name: "cccc"}],
        contentType: {
            name: "wwww"
        },
        locales: {
            name: "en-US",
            description: "eeee",
            surname: {
                zxcvb: "bvcxz"
            }
        }
    },
    {
        name: "Mazda",
        description: [{name: "ddddd"}, {name: "eeee"}, {name: "ffff"}],
        contentType: {
            name: "zzzz"
        },
        locales: {
            name: "ru-RU",
            description: "yyyy"
        }
    },
    {
        name: "Opel",
        description: {name: "gggg"},
        contentType: {
            name: "rrrr"
        },
        locales: {
            name: "en-US",
            description: "tttt"
        }
    }
];

const resultArr = filterCollection(vehicles, "cccccc en-us toyota", true, "description.surname", "description.name", "locales.name", "name");
resultArr.length ? console.log(resultArr) : console.log("No matches!");