const {src, dest} = require("gulp");
const jsMinify = require("gulp-js-minify");
const concat = require("gulp-concat");

const scripts = () => {
    return src("./src/js/*.js")
        .pipe(concat("scripts.min.js"))
        .pipe(dest("./dist/js/"));
};

const scriptsMin = () => {
    return src("./src/js/*.js")
        .pipe(concat("scripts.min.js"))
        .pipe(jsMinify())
        .pipe(dest("./dist/js/"));
};

exports.scripts = scripts;
exports.scriptsMin = scriptsMin;