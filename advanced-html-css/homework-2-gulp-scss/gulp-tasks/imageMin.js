const {src, dest} = require("gulp");
const newer = require('gulp-newer');
const compress = require("gulp-imagemin");

const imageMin = () => {
    return src("./src/img/*")
        .pipe(newer("./dist/img"))
        .pipe(compress([
            compress.mozjpeg({quality: 75, progressive: true}),
            compress.optipng({optimizationLevel: 5})]))
        .pipe(dest("./dist/img"))
};

exports.imageMin = imageMin;