"use strict"

class Server {
    constructor(url) {
        this.url = url;
    }

    async request(method, queryType = "", queryNumber = "", data) {
        const response = await fetch(`${this.url}/${queryType}/${queryNumber}`, {
            method: method,
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        });
        const value = await response.json();
        if (!response.ok) {
            throw new Error(value);
        }
        return value;
    }
}

// npx json-server --watch db.json
// const baseURL = "http://localhost:3000";
const baseURL = "https://jsonplaceholder.typicode.com";

const server = new Server(baseURL);
const users = [];
const posts = [];
const addBtn = document.querySelector(".add-post-btn");
const addPostForm = document.querySelector(".add-post-form");
addBtn.addEventListener("click", addBtnClickHandler);
addPostForm.addEventListener("submit", addPost);


initializePage().catch(error => alert(error));


async function initializePage() {
    const postsData = await getDataForRender(posts, users);
    document.body.querySelector(".posts-section > .loader").remove();
    renderPosts(postsData);
}

async function getDataForRender(posts, users) {
    const [postsData, usersData] = await Promise.all([server.request("GET", "posts"), server.request("GET", "users")]);
    posts.push(...postsData);
    users.push(...usersData);
    return posts.map(({id, title, body, userId}) => {
        const user = users.find(({id}) => id === userId);
        const {name: userName, email: userEmail} = user;
        return {id, title, body, userName, userEmail};
    })
}

async function addPost(event) {
    try {
        event.preventDefault();
        const [titleField, textField] = this.elements;
        const postData = {title: titleField.value, body: textField.value, userId: 1};
        const responseValue = await server.request("POST", "posts", "", postData);
        const {name: userName, email: userEmail} = users[0];
        renderPosts([{...responseValue, userName, userEmail}]);
        const modalOverlay = document.querySelector(".modal-overlay");
        modalOverlay.classList.remove("active");
    } catch (error) {
        alert(error);
    }
}

function addBtnClickHandler() {
    const modalOverlay = document.querySelector(".modal-overlay");
    modalOverlay.classList.add("active");
    const titleField = modalOverlay.querySelector(".post-title");
    const textField = modalOverlay.querySelector(".post-text");
    titleField.value = textField.value = "";
    modalOverlay.querySelector(".post-title").focus();
}

class Post {
    constructor({id, title, body, userName, userEmail}) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.userName = userName;
        this.userEmail = userEmail;
    }

    create() {
        const postWrapper = this._createPostWrapper();
        const postTitle = this._createPostTitle();
        const postBody = this._createPostBody();
        const postAuthor = document.createElement("p");
        const postUserName = this._createPostLink(this.userName);
        const postUserEmail = this._createPostLink(this.userName, `mailto:${this.userEmail}`);
        postAuthor.append(postUserName, postUserEmail);
        const editBtn = this._createPostBtn("Edit", this._editBtnClickHandler);
        const delBtn = this._createPostBtn("Delete", this._delBtnClickHandler);
        postWrapper.append(postAuthor, postTitle, postBody, editBtn, delBtn);
        return postWrapper;
    }

    _createPostWrapper() {
        const postWrapper = document.createElement("article");
        postWrapper.classList.add("post");
        postWrapper.setAttribute("data-post-id", this.id);
        return postWrapper;
    }

    _createPostTitle() {
        const postTitle = document.createElement("textarea");
        postTitle.classList.add("post-title");
        postTitle.value = this.title;
        postTitle.setAttribute("readonly", "");
        return postTitle;
    }

    _createPostBody() {
        const postBody = document.createElement("textarea");
        postBody.classList.add("post-text");
        postBody.value = this.body;
        postBody.setAttribute("readonly", "");
        return postBody;
    }

    _createPostLink(linkText, href = "#") {
        const postLink = document.createElement("a");
        postLink.setAttribute("href", href);
        postLink.textContent = linkText;
        return postLink;
    }

    _createPostBtn(btnText, btnClickHandler) {
        const btn = document.createElement("button");
        btn.classList.add("post-btn");
        btn.setAttribute("type", "button");
        btn.textContent = btnText;
        btn.addEventListener("click", btnClickHandler);
        return btn;
    }

    async _editBtnClickHandler() {
        const post = this.closest("[data-post-id]");
        const postId = post.getAttribute("data-post-id");
        const postTitle = post.querySelector(".post-title");
        const postBody = post.querySelector(".post-text");
        if (!this.classList.contains("editing")) {
            this.textContent = "Save";
            this.classList.add("editing");
            postTitle.removeAttribute("readonly");
            postBody.removeAttribute("readonly");
            postTitle.classList.add("editable");
            postBody.classList.add("editable");
            postTitle.focus();
        } else try {
            const postData = {userId: 1, title: postTitle.value, body: postBody.value};
            await server.request("PUT", "posts", postId, postData);
            postTitle.setAttribute("readonly", "");
            postBody.setAttribute("readonly", "");
            postTitle.classList.remove("editable");
            postBody.classList.remove("editable");
            this.textContent = "Edit";
            this.classList.remove("editing");
        } catch (error) {
            alert(error);
        }
    }

    async _delBtnClickHandler() {
        try {
            const post = this.closest("[data-post-id]");
            const postId = post.getAttribute("data-post-id");
            await server.request("DELETE", "posts", postId);
            post.remove();
        } catch (error) {
            alert(error);
        }
    }
}

function renderPosts(data) {
    const postsSection = document.body.querySelector(".posts-section");
    data.forEach(postData => {
        const postCard = new Post(postData).create();
        postsSection.prepend(postCard);
    })
}