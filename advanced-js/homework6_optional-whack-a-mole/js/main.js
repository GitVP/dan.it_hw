"use strict"

class WhackAMoleGame {
    constructor() {
        this.table = this._createTable();
        this._handleCellClick();
        this.playerScore = 0;
        this.compScore = 0;
        this.intervalTime = 0;
    }

    startGame() {
        document.body.prepend(this.table);
        this.gameTimer = setInterval(() => this._setMole(), this.intervalTime);
    }

    resetGame() {
        this.table.classList.remove("inactive");
        const cells = this.table.querySelectorAll("td");
        cells.forEach(cell => cell.className = "");
        this.playerScore = 0;
        this.compScore = 0;
        this.gameTimer = setInterval(() => this._setMole(), this.intervalTime);
    }

    _createTable() {
        const table = document.createElement("table");
        table.className = "game-table";
        for (let i = 0; i < 10; i++) {
            const tr = document.createElement("tr");
            for (let j = 0; j < 10; j++) {
                const td = document.createElement("td");
                tr.append(td);
            }
            table.append(tr);
        }
        return table;
    }

    _handleCellClick() {
        this.table.addEventListener("click", (event) => {
            if (event.target.tagName === "TD") {
                const cell = event.target;
                if (cell.classList.contains("active")) {
                    cell.classList.remove("active");
                    cell.classList.add("whacked");
                    this.playerScore++;
                    this._checkWin();
                }
            }
        })
    }

    _setMole() {
        let cell;
        while (!cell) {
            const positionX = Math.floor(Math.random() * (10));
            const positionY = Math.floor(Math.random() * (10));
            const checkedCell = this.table.rows[positionX].cells[positionY];
            if (!checkedCell.classList.contains("whacked") && !checkedCell.classList.contains("moled")) cell = checkedCell;
        }
        cell.classList.add("active");
        setTimeout(() => {
            if (cell.classList.contains("active")) {
                cell.classList.remove("active");
                cell.classList.add("moled");
                this.compScore++;
                this._checkWin();
            }
        }, this.intervalTime - 10);
    }

    _checkWin() {
        if (this.compScore >= 50 || this.playerScore >= 50) {
            clearInterval(this.gameTimer);
            const gameDiff = document.querySelector(".game-diff");
            gameDiff.classList.remove("hided");
            this.table.classList.add("inactive");
            const winMsg = this.compScore > this.playerScore ? "Computer won!" : "You won!";
            setTimeout(() => alert(winMsg), 10);
            return true;
        }
    }
}

const game = new WhackAMoleGame();
const startBtn = document.querySelector("#start-button");
startBtn.addEventListener("click", startClickHAndler);

function startClickHAndler() {
    const gameDiff = document.querySelector(".game-diff");
    const diffSelect = gameDiff.querySelector("select");
    game.intervalTime = diffSelect.value;
    gameDiff.classList.add("hided");
    if (document.querySelector(".game-table")) {
        game.resetGame();
    } else {
        game.startGame();
    }
}