"use strict"


const filmURL = "https://swapi.dev/api/films/";

fetch(filmURL)
    .then(response => response.json())
    .then(data => {
        const charArr = getCharacters(data);
        renderFilms(data);
        const loaderGears = document.querySelectorAll(".gear");
        charArr.forEach(renderCharacters(loaderGears));
    })
    .catch(error => console.log(error))


function getCharacters({results}) {
    return results.map(({characters}) => characters);
}

function renderFilms({results}) {
    const [ul, li] = createList();
    results.forEach((item) => {
        const {title, episode_id, opening_crawl} = item;
        const listItem = li.cloneNode(true);
        listItem.children[0].textContent = episode_id;
        listItem.children[1].textContent = title;
        listItem.children[2].classList.add("gear");
        listItem.children[3].textContent = opening_crawl;
        ul.append(listItem);
    })
    document.body.prepend(ul);
}

function createList() {
    const ul = document.createElement("ul");
    const li = document.createElement("li");
    const episode = document.createElement("p");
    const title = document.createElement("h3");
    const characters = document.createElement("p");
    const openingCrawl = document.createElement("p");
    li.append(episode, title, characters, openingCrawl);
    return [ul, li]
}

function renderCharacters(loaderGears) {
    return function (heroList, filmIndex) {
        const fetchArr = heroList.map(heroURL => {
            return fetch(heroURL)
                .then(response => response.json())
                .catch(error => console.log(error))
        });
        Promise.all(fetchArr).then((data) => {
            const heroNames = data.map(({name}) => name);
            const ul = document.createElement("ul");
            heroNames.forEach(item => {
                const li = document.createElement("li");
                li.textContent = item;
                ul.append(li);
            });
            loaderGears[filmIndex].append(ul);
            loaderGears[filmIndex].classList.remove("gear");
        });
    }
}