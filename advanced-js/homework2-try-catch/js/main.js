"use strict"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const list = document.createElement("ul");
const root = document.querySelector("#root");


books.forEach(({author, name, price}) => {
    try {
        let errorMsg = "";
        if (!author) errorMsg += "no author ";
        if (!name) errorMsg += "no name ";
        if (!price && price !== 0) errorMsg += "no price ";
        if (errorMsg !== "") throw new SyntaxError(`${author} ${name} ${price} - ${errorMsg}`);
        const li = document.createElement("li");
        li.textContent = JSON.stringify({author, name, price}).slice(1, -1);
        list.appendChild(li);
    } catch (e) {
        if (e.name === "SyntaxError") {
            console.log(`Incomplete data: ${e.message}`);
        } else console.log(e);
    }
});

root.append(list);