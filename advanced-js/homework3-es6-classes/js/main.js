"use strict"

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(name) {
        this._name = name;
    }

    get name() {
        return this._name;
    }

    set age(age) {
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(salary) {
        this._salary = salary * 3;
    }

    get salary() {
        return this._salary;
    }
}


const vasya = new Programmer("vasya", 12, 1000, ["eng", "rus"]);
const petya = new Programmer("petya", 15, 1500, ["eng", "ukr"]);
const johnD = new Programmer("john", 20, 2300, ["rus", "ukr"]);
console.log(vasya, petya, johnD);