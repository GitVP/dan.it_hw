import React, {useState, useEffect} from "react";
import "./GoodsItem.scss"
import {ReactComponent as FavStar} from "./icons/fav-star.svg";
import {ReactComponent as Cart} from "./icons/cart.svg";
import {ReactComponent as CartFilled} from "./icons/cart-filled.svg";
import Modal from "../Modal/Modal";
import LocalStorage from "../../services/LocalStorage/LocalStorage";
import PropTypes from "prop-types";

const storage = new LocalStorage();

function GoodsItem(props) {
    const [isInFavorite, setIsInFavorite] = useState(false);
    const [isInCart, setIsInCart] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalHeader, setModalHeader] = useState(null);


    useEffect(() => {
        const {honey: {id}} = props;
        if (storage.favorites && storage.favorites.some(storedId => storedId === id)) setIsInFavorite(true);
        if (storage.addedToCart && storage.addedToCart.some(storedId => storedId === id)) setIsInCart(true);
    }, [props]);


    const closeModal = (event) => {
        if (event.target.dataset.action === "close-modal") {
            setModalVisible(false);
        }
    }

    const addToCart = () => {
        storage.add("addedToCart", props.honey.id);
        setModalVisible(false);
        setIsInCart(true);
    }

    const removeFromCart = () => {
        storage.remove("addedToCart", props.honey.id);
        if (props.updCart) props.updCart([...storage.addedToCart]);
        setModalVisible(false);
        setIsInCart(false);
    }

    const cartBtnClickHandler = () => {
        const headerText = isInCart ? "Удалить товар из корзины?" : "Добавить товар в корзину?";
        setModalHeader(headerText);
        setModalVisible(true);
    }

    const favoriteBtnClickHandler = () => {
        const {honey: {id}} = props;
        if (!isInFavorite) {
            storage.add("favorites", id);
        } else {
            storage.remove("favorites", id);
            if (props.updFavorites) props.updFavorites([...storage.favorites]);
        }
        setIsInFavorite(!isInFavorite);
    }

    const {honey: {vendorCode, name, price, imgURL, color}} = props;

    return (
        <>
            <li className="honey-list__item card">
                <button className={`fav-btn${isInFavorite ? " added" : ""}`} onClick={favoriteBtnClickHandler}>
                    <FavStar/>
                </button>
                <a className="card__link" href="/">
                    <div className="card__img-wrapper">
                        <img className="card__img" src={imgURL} alt={name}/>
                    </div>
                    <h3 className="card__title">{name}</h3>
                </a>
                <p className="card__info">
                    <span>Артикул: {vendorCode} </span>
                    <span>Цвет: {color}</span>
                </p>
                <p className="card__purchase">
                    {price}₴
                    <button className="cart-btn" onClick={cartBtnClickHandler}>
                        {isInCart ? <CartFilled/> : <Cart/>}
                    </button>
                </p>
            </li>
            {modalVisible && <Modal
                header={modalHeader}
                closeButton={true}
                text={`Вы действительно хотите ${modalHeader.toLowerCase()}`}
                actions={
                    <div className="cart-confirm-wrapper">
                        <button className="cart-confirm-btn cart-confirm-btn--accept" onClick={isInCart ? removeFromCart: addToCart} data-action="close-modal">ОК</button>
                        <button className="cart-confirm-btn cart-confirm-btn--decline" onClick={closeModal} data-action="close-modal">Отмена</button>
                    </div>
                }
                closeHandler={closeModal}
            />}
        </>
    );
}

GoodsItem.propTypes = {
    honey: PropTypes.object.isRequired,
    updCart: PropTypes.func,
    updFavorites: PropTypes.func
}

export default GoodsItem;