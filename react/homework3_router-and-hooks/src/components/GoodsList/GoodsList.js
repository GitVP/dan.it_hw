import React from "react";
import GoodsItem from "../GoodsItem/GoodsItem";
import PropTypes from "prop-types";
import "./GoodsList.scss";

function GoodsList({honeyArr, updCart, updFavorites}) {
    const honeyCards = honeyArr.map(h => <GoodsItem key={h.id} honey={h} updCart={updCart} updFavorites={updFavorites}/>);
    return (
        <ul className="honey-list">
            {honeyCards}
            {!honeyCards.length && <li className="honey-list__empty-state">No items</li>}
        </ul>
    );
}

GoodsList.propTypes = {
    honeyArr: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        vendorCode: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        imgURL: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired
    })).isRequired
}

export default GoodsList;