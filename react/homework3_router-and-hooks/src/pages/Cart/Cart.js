import React, {useEffect, useState} from "react";
import LocalStorage from "../../services/LocalStorage/LocalStorage";
import GoodsList from "../../components/GoodsList/GoodsList";


function Cart({honeyArr}) {
    const [cartedArr, setCartedArr] = useState([]);

    useEffect(() => {
        const storage = new LocalStorage();
        setCartedArr(storage.addedToCart || []);
    }, []);

    const cartedItems = honeyArr.filter((h, i) => cartedArr.includes(i + 1));
    return <GoodsList honeyArr={cartedItems} updCart={setCartedArr}/>
}

export default Cart;