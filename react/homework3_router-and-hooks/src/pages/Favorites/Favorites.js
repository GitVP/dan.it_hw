import React, {useEffect, useState} from "react";
import LocalStorage from "../../services/LocalStorage/LocalStorage";
import GoodsList from "../../components/GoodsList/GoodsList";


function Favorites({honeyArr}) {
    const [favoritesArr, setFavoritesArr] = useState([]);

    useEffect(() => {
        const storage = new LocalStorage();
        setFavoritesArr(storage.favorites || []);
    }, []);

    const favoritesItems = honeyArr.filter((h, i) => favoritesArr.includes(i + 1));
    return <GoodsList honeyArr={favoritesItems} updFavorites={setFavoritesArr}/>
}

export default Favorites;