import React from "react";
import {Route, Switch} from "react-router-dom";
import GoodsList from "../components/GoodsList/GoodsList";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";

function AppRoutes(props) {
    const {honeyArr} = props;
    return (
        <Switch>
            <Route exact path="/"><GoodsList honeyArr={honeyArr}/></Route>
            <Route exact path="/cart"><Cart honeyArr={honeyArr}/></Route>
            <Route exact path="/favorites"><Favorites honeyArr={honeyArr}/></Route>
        </Switch>
    )
}

export default AppRoutes
