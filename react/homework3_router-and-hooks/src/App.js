import React, {useState, useEffect} from "react";
import Navbar from "./components/Navbar/Navbar";
import AppRoutes from "./routes/AppRoutes";
import API from "./services/API/API";
import "./styles/App.scss";


function App() {
    const [honeyArr, setHoneyArr] = useState([]);

    useEffect(() => {
        API.getHoneyList().then(({data: honeyArr}) => setHoneyArr(honeyArr));
    }, []);

    return (
        <>
            <header>
                <div className="container">
                    <h1 className="title">Online Honey Shop</h1>
                    <Navbar/>
                </div>
            </header>
            <main>
                <div className="container">
                    <AppRoutes honeyArr={honeyArr}/>
                </div>
            </main>
        </>
    );
}

export default App;