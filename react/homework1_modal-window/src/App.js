import React, {PureComponent} from 'react';
import './styles/App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";


class App extends PureComponent {
    state = {
        firstModalVisible: false,
        secondModalVisible: false
    }

    toggleFirstModal = (event) => {
        if (event.target.dataset.action === "toggle-modal") {
            this.setState({firstModalVisible: !this.state.firstModalVisible});
        }
    }

    toggleSecondModal = (event) => {
        if (event.target.dataset.action === "toggle-modal") {
            this.setState({secondModalVisible: !this.state.secondModalVisible});
        }
    }

    render() {
        const firstModal = <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            text="Once you delete this file, it won’t be possible to undo this action.
Are you sure you want to delete it?"
            actions={
                <>
                    <Button backgroundColor="#b3382c" text="Ok"/>
                    <Button backgroundColor="#b3382c" text="Cancel"/>
                </>
            }
            closeHandler={this.toggleFirstModal}
        />;
        const secondModal = <Modal
            header="Lorem ipsum dolor sit amet, consectetur adipisicing"
            closeButton={false}
            text="A ab asperiores aspernatur aut debitis fuga ipsa itaque, iure laborum nam, nulla obcaecati odit optio placeat qui. Quod repellendus sapiente sit unde voluptate?"
            actions={
                <>
                    <Button backgroundColor="#b5b5fb" text="Agree"/>
                    <Button backgroundColor="#b5b5fb" text="Remind me later"/>
                </>
            }
            closeHandler={this.toggleSecondModal}
        />;

        const {firstModalVisible, secondModalVisible} = this.state;
        return (
            <div className="App">
                <Button backgroundColor="#ffeeed" text="Open first modal" onClick={this.toggleFirstModal}/>
                <Button backgroundColor="#fdc49c" text="Open second modal" onClick={this.toggleSecondModal}/>
                {firstModalVisible && firstModal}
                {secondModalVisible && secondModal}
            </div>
        );
    }
}

export default App;