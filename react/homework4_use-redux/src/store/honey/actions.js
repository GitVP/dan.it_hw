import {SET_HONEY} from "./types";

export const setHoney = (honeyArr) => (dispatch) => {
    dispatch({type: SET_HONEY, payload: honeyArr});
}