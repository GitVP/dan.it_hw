import API from "../../services/API/API";
import {setHoney} from "./actions";

export const getHoney = () => (dispatch) => {
    API.getHoneyList().then(res => dispatch(setHoney(res.data)));
}