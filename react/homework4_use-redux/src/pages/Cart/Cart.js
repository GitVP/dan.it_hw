import React from "react";
import GoodsList from "../../components/GoodsList/GoodsList";
import {connect} from "react-redux";


function Cart({honeyArr, cartedArr}) {
    const cartedItems = honeyArr.filter((h, i) => cartedArr.includes(i));
    return <GoodsList honeyArr={cartedItems}/>
}

const mapStateToProps = (state) => {
    return {
        honeyArr: state.honey,
        cartedArr: state.cartedArr
    }
}

export default connect(mapStateToProps)(Cart)