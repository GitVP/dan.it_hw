import React, {PureComponent} from 'react';
import GoodsList from "./components/GoodsList/GoodsList";
import axios from "axios";
import "./styles/App.scss";

class App extends PureComponent {
    state = {
        honeyArr: [],
    }

    async componentDidMount() {
        const {data: honeyArr} = await axios("/honey.json");
        this.setState({honeyArr});
    }

    render() {
        const {honeyArr} = this.state;
        return (
            <div className="container">
                <h1 className="title">Online Honey Shop</h1>
                <GoodsList honeyArr={honeyArr}/>
            </div>
        );
    }
}

export default App;