import React, {PureComponent} from "react";
import "./GoodsItem.scss"
import {ReactComponent as FavStar} from "./fav-star.svg";
import {ReactComponent as Cart} from "./cart.svg";
import {ReactComponent as CartFilled} from "./cart-filled.svg";
import Modal from "../Modal/Modal";
import LocalStorage from "../LocalStorage/LocalStorage";
import PropTypes from "prop-types";

const storage = new LocalStorage();

class GoodsItem extends PureComponent {
    state = {
        isInFavorite: false,
        isInCart: false,
        modalVisible: false
    }

    componentDidMount() {
        const {honey: {id}} = this.props;
        if (storage.favorites && storage.favorites.some(storedId => storedId === id)) this.setState({isInFavorite: true});
        if (storage.addedToCart && storage.addedToCart.some(storedId => storedId === id)) this.setState({isInCart: true});
    }

    closeModal = (event) => {
        if (event.target.dataset.action === "close-modal") {
            this.setState({modalVisible: false});
        }
    }

    addToCart = () => {
        storage.add("addedToCart", this.props.honey.id);
        this.setState({modalVisible: false, isInCart: true});
    }

    cartBtnClickHandler = () => {
        const {isInCart} = this.state;
        if (isInCart) {
            storage.remove("addedToCart", this.props.honey.id);
            this.setState({isInCart: false})
        } else {
            this.setState({modalVisible: true})
        }
    }

    favoriteBtnClickHandler = () => {
        const {isInFavorite} = this.state;
        const {honey: {id}} = this.props;
        if (!isInFavorite) {
            storage.add("favorites", id);
        } else {
            storage.remove("favorites", id);
        }
        this.setState({isInFavorite: !this.state.isInFavorite});
    }

    render() {
        const {honey: {vendorCode, name, price, imgURL, color}} = this.props;
        const {isInFavorite, isInCart, modalVisible} = this.state;
        return (
            <>
                <li className="honey-list__item card">
                    <button className={`fav-btn${isInFavorite ? " added" : ""}`} onClick={this.favoriteBtnClickHandler}>
                        <FavStar/>
                    </button>
                    <a className="card__link" href="#">
                        <div className="card__img-wrapper">
                            <img className="card__img" src={imgURL} alt={name}/>
                        </div>
                        <h3 className="card__title">{name}</h3>
                    </a>
                    <p className="card__info">
                        <span>Артикул: {vendorCode} </span>
                        <span>Цвет: {color}</span>
                    </p>
                    <p className="card__purchase">
                        {price}₴
                        <button className="cart-btn" onClick={this.cartBtnClickHandler}>
                            {isInCart ? <CartFilled/> : <Cart/>}
                        </button>
                    </p>
                </li>
                {modalVisible && <Modal
                    header="Добавить в корзину?"
                    closeButton={true}
                    text="Вы действительно хотите добавить товар в корзину?"
                    actions={
                        <div className="cart-confirm-wrapper">
                            <button className="cart-confirm-btn cart-confirm-btn--accept" onClick={this.addToCart} data-action="close-modal">ОК</button>
                            <button className="cart-confirm-btn cart-confirm-btn--decline" onClick={this.closeModal} data-action="close-modal">Отмена</button>
                        </div>
                    }
                    closeHandler={this.closeModal}
                />}
            </>
        );
    }
}

GoodsItem.propTypes = {
    honey: PropTypes.object.isRequired
}

export default GoodsItem;