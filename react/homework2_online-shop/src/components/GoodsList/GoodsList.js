import React, {PureComponent} from "react";
import GoodsItem from "../GoodsItem/GoodsItem";
import PropTypes from "prop-types";
import "./GoodsList.scss";

class GoodsList extends PureComponent {
    render() {
        const {honeyArr} = this.props;
        const honeyCards = honeyArr.map(h => <GoodsItem key={h.id} honey={h}/>);
        return (
            <ul className="honey-list">
                {honeyCards}
            </ul>
        );
    }
}

GoodsList.propTypes = {
    honeyArr: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        vendorCode: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        imgURL: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired
    })).isRequired
}

export default GoodsList;